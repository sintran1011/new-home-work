import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import Me from "./reducers/Me";
import AccountState from "./reducers/AccountState";
import UserDBReducer from "./reducers/UserDBReducer";
import DataDocument from "./reducers/DataDocument";

const rootReducer = combineReducers({
  Me,
  AccountState,
  UserDBReducer,
  DataDocument,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk))
);

export default store;
