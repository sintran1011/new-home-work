import { actionType } from "../actions/type";

const initialState = {
  isLogin: false,
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case actionType.SET_ADMIN_ISLOGIN:
      state.isLogin = true;
      return { ...state };

    case actionType.SET_ADMIN_ISLOGOUT:
      state.isLogin = false;
      return { ...state };

    default:
      return state;
  }
};

export default reducer;
