const initialState = {
  arrUserDB: {},
  userGoogle: {},
  idUser: {},
};
export default (state = initialState, action) => {
  switch (action.type) {
    case "GET_DSUSER": {
      state.arrUserDB = action.arrUserDB;
      // console.log(" reducer", state.arrUserDB);
      return { ...state };
    }
    case "LOGIN_GOOGLE": {
      // const { infoLoginGoolge } = action;
      return { ...state, userGoogle: action.infoLoginGoolge };
    }
    default:
      return { ...state };
  }
};
