import { actionType } from "../actions/type";

const initialState = {
  // render document
  dataDocument: [],
  dataDocumentPages: 0,
  dataDocumentPageIndex: 1,

  // state for modal control
  editDocumentState: false,
  detailDocumentState: false,
  assignDocumentState: false,

  // id for content modal
  editDocumentId: "",
  assignDocumentId: "",
  //

  detailDocument: "",
  userListAssign: "",
  deleteDocumentList: "",
  loadingState: false,
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    // Document
    case actionType.SET_DOCUMENT:
      state.dataDocument = payload.documents;
      state.dataDocumentPages = payload.pages;
      return { ...state };

    case actionType.SET_PAGE_INDEX:
      state.dataDocumentPageIndex = payload;
      return { ...state };

    //Detail
    case actionType.SET_DOC_DETAIL:
      state.detailDocument = payload;
      return { ...state };

    case actionType.SET_DETAIL_DOCUMENT_STATE:
      state.detailDocumentState = payload;
      return { ...state };

    // Edit
    case actionType.SET_EDIT_DOCUMENT_STATE:
      state.editDocumentState = payload.state;
      state.editDocumentId = payload.id;
      return { ...state };

    // Assign
    case actionType.SET_ASSIGN_DOCUMENT_STATE:
      state.assignDocumentState = payload.state;
      state.assignDocumentId = payload.id;
      return { ...state };

    case actionType.SET_USER_LIST_ASSIGN:
      state.userListAssign = payload;
      return { ...state };

    // Closed modal clear data
    case actionType.SET_CLEAR_MODAL_CONTENT_STATE:
      state.detailDocumentState = false;
      state.editDocumentState = false;
      state.assignDocumentState = false;
      return { ...state };

    // loader
    case actionType.SET_LOADING_OPEN:
      state.loadingState = true;
      return { ...state };

    case actionType.SET_LOADING_CLOSED:
      state.loadingState = false;
      return { ...state };

    //Render delete list
    case actionType.SET_DELETED_DOCUMENT_LIST:
      state.deleteDocumentList = payload.documents;
      state.dataDocumentPages = payload.pages;
      return { ...state };

    default:
      return state;
  }
};

export default reducer;
