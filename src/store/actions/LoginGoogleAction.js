import { UserGoogle } from "../../services/UserGoogle";

export const loginGoogleAction = (infoLoginGoolge, navigate) => {
  return async (dispatch) => {
    try {
      const rs = await UserGoogle.GGsignin(infoLoginGoolge);
      localStorage.setItem("token", rs.data.token);
      dispatch({
        type: "LOGIN_GOOGLE",
        infoLoginGoolge: rs.data,
      });
      console.log("cai token", rs.data);
      navigate("/User", { replace: true });
    } catch (error) {
      console.log(error.response);
    }
  };
};
