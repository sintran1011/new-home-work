import { ModalView } from "../../services/ModalViewUS";

export const ModalViewUser = (docId, status) => {
  return async (dispatch) => {
    try {
      const rs = await ModalView.popupView(docId, status);

      if (rs.status === 200) {
        alert("Update success!");

        window.location.reload();
      }
      console.log("data trong action", rs.data);
    } catch (error) {
      console.log(error.response);
    }
  };
};
