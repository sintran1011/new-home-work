import createAction from ".";
import {
  allowUserReadDoc,
  assignDoc,
  createNewDoc,
  deleteDocById,
  getAllDoc,
  getDocById,
  getTrash,
  updateDocById,
} from "../../services/document";
import { actionType } from "./type";

//renderTable
export const fetchDoc = (perPage, pageIndex) => {
  return async (dispatch) => {
    try {
      dispatch(createAction(actionType.SET_LOADING_OPEN));

      const res = await getAllDoc(perPage, pageIndex);
      if (res.status === 200) {
        dispatch(createAction(actionType.SET_DOCUMENT, res.data));
        console.log(res);
      }
    } catch (err) {
      console.log(err);
    } finally {
      dispatch(createAction(actionType.SET_LOADING_CLOSED));
    }
  };
};

//render detail doc by ID
export const fetchDetailDoc = (id) => {
  return async (dispatch) => {
    try {
      const res = await getDocById(id);
      if (res.status === 200) {
        dispatch(createAction(actionType.SET_DOC_DETAIL, res.data));
        console.log(res.data);
      }
    } catch (err) {
      console.log(err);
    }
  };
};

//delete doc by ID
export const deleteDoc = (id, perPage, pageIndex) => {
  return async (dispatch) => {
    try {
      dispatch(createAction(actionType.SET_LOADING_OPEN));
      const res = await deleteDocById(id);
      if (res.status === 200) {
        console.log("deleteDoc", res.data);
        alert("Delete file success");
      }
    } catch (err) {
      console.log("deleteDoc", err);
    } finally {
      dispatch(fetchDoc(perPage, pageIndex));
      dispatch(createAction(actionType.SET_LOADING_CLOSED));
    }
  };
};

//get all delete docList
export const fetchTrash = (perPage, pageIndex) => {
  return async (dispatch) => {
    try {
      dispatch(createAction(actionType.SET_LOADING_OPEN));
      const res = await getTrash(perPage, pageIndex);
      if (res.status === 200) {
        console.log("getDeletedDocList", res.data);
        dispatch(createAction(actionType.SET_DELETED_DOCUMENT_LIST, res.data));
      }
    } catch (err) {
      console.log("getDeletedDocList", err);
    } finally {
      dispatch(createAction(actionType.SET_LOADING_CLOSED));
    }
  };
};

//update doc by ID
export const updateDoc = (
  id,
  formData,
  dataDocumentPerPage,
  dataDocumentPageIndex
) => {
  return async (dispatch) => {
    try {
      const res = await updateDocById(id, formData);
      if (res.status === 200) {
        console.log("updateDocById", res);
        alert("Update file success");
        dispatch(fetchDoc(dataDocumentPerPage, dataDocumentPageIndex));
      }
    } catch (err) {
      console.log("updateDoc", err);
    }
  };
};

// add new doc
export const addNewDoc = (data, perPage, pageIndex) => {
  return async (dispatch) => {
    try {
      dispatch(createAction(actionType.SET_LOADING_OPEN));
      const res = await createNewDoc(data);
      if (res.status === 200) {
        console.log("addNewDoc", res.data);
        alert("add new doc successful");
      }
    } catch (err) {
      console.log("addNewDoc", err);
    } finally {
      dispatch(fetchDoc(perPage, pageIndex));
      dispatch(createAction(actionType.SET_LOADING_CLOSED));
    }
  };
};

//get userListAssign
export const getUserListAssign = (id, state) => {
  return async (dispatch) => {
    try {
      dispatch(createAction(actionType.SET_LOADING_OPEN));
      const res = await assignDoc(id, state);
      if (res.status === 200) {
        console.log("getUserListAssign", res.data);
        dispatch(createAction(actionType.SET_USER_LIST_ASSIGN, res.data));
      }
    } catch (err) {
      console.log("getListUser", err);
    } finally {
      dispatch(createAction(actionType.SET_LOADING_CLOSED));
    }
  };
};

//post userList can read doc
export const updateListUserReadDoc = (userData, id, state) => {
  return async (dispatch) => {
    try {
      const res = await allowUserReadDoc(userData);
      console.log(res);
    } catch (err) {
      console.log("updateListUserReadDoc", err);
    } finally {
      dispatch(getUserListAssign(id, state));
    }
  };
};
