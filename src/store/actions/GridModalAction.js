import { GridmodalView } from "../../services/GridModalUS.js";

export const GridModalAction = (docId, status) => {
  return async (dispatch) => {
    try {
      const rs = await GridmodalView.GirdpopupView(docId, status);

      if (rs.status === 200) {
        alert("Update success!");

        window.location.reload();
      }
      console.log("data trong action", rs.data);
    } catch (error) {
      console.log(error.response);
    }
  };
};
