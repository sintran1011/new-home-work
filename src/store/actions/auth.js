import createAction from ".";
import { actionType } from "./type";
import { accountInfo } from "../../services/adminAccount";
import { signinAdmin } from "../../services/adminAccount";

export const adminSignin = (userData, navigate) => {
  return async (dispatch) => {
    try {
      const res = await signinAdmin(userData);

      console.log(res.data);
      if (res.status === 200) {
        localStorage.setItem("token", res.data.token);
        dispatch(createAction(actionType.SET_ADMIN_ISLOGIN))
      }
    } catch (err) {
      console.log("Signin", err);
    } finally {
      navigate("/Admin");
    }
  };
};

// export const adminSignin = (userData, navigate) => {
//   return async (dispatch) => {
//     function onSuccess(success) {
//       localStorage.setItem("token", success.data.token);
//       dispatch(createAction(actionType.SET_ADMIN, success.data));
//       dispatch(createAction(actionType.SET_ADMIN_ISLOGIN));
//       return success;
//     }
//     function onError(error) {
//       // dispatch({ type: ERROR_GENERATED, error });
//       return error;
//     }
//     try {
//       const success = await signinAdmin(userData);
//       onSuccess(success);
//       return navigate("/Admin")
//     } catch (error) {
//       console.log("Signin", error);
//       return onError(error);
//     }
//   };
// };

export const fetchMe = async (dispatch) => {
  try {
    const res = await accountInfo();
    if (res.status === 200) {
      dispatch(createAction(actionType.SET_ME, res.data));
      console.log(res);
      // dispatch(createAction(actionType.SET_ADMIN_ISLOGIN));
    }
  } catch (err) {
    console.log("fetchMe", err);
  }
};
