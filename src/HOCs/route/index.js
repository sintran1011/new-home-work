import { Navigate } from "react-router-dom";

export const AuthRoute = ({ navigativePath, children }) => {
  const token = localStorage.getItem("token");
  if (token) {
    return children;
  }
  return <Navigate to={navigativePath} />;
};

export const LoginRoute = ({ navigativePath, children }) => {
  const token = localStorage.getItem("token");
  if (!token) {
    return children;
  }
  return <Navigate to={navigativePath} />;
};
