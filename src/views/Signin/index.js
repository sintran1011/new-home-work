import React from "react";
import style from "./style.module.css";
import { useFormik } from "formik";
import * as yup from "yup";
import { useDispatch } from "react-redux";
import { adminSignin } from "../../store/actions/auth";
import { GoogleLogin, GoogleLogout } from "react-google-login";
import { useNavigate } from "react-router-dom";
import { loginGoogleAction } from "../../store/actions/LoginGoogleAction";
const schema = yup.object().shape({
  username: yup.string().required("Please type your username"),
  password: yup.string().required("Please type your password"),
});

const Signin = () => {
  const navigate = useNavigate();

  const formik = useFormik({
    initialValues: {
      username: "",
      password: "",
    },
    validationSchema: schema,
    validateOnMount: true,
  });

  //login bang google
  const responseGoogle = (response) => {
    dispatch(loginGoogleAction({ tokenId: response.tokenId }, navigate));
    // console.log(response);
  };

  const onSuccess = () => {
    alert("Logout made successfully");
  };
  // submit to login
  const handleSubmit = async (e) => {
    e.preventDefault();
    dispatch(adminSignin(formik.values, navigate));
  };

  const dispatch = useDispatch();

  return (
    // <Fragment>
    //   {token ? (
    //     <Navigate to="/Admin" replace />
    //   ) : (
    <div className="container d-flex flex-row">
      <div className={`${style["bg-left"]} col-xl-6`}></div>
      <div className={`col-xl-6 ${style["content"]}`}>
        <form onSubmit={handleSubmit}>
          <h1 className="text-center">Sign in</h1>

          <div className="mb-3">
            <label htmlFor="inputUsername" className="form-label">
              Username
            </label>
            <div className="input-group">
              <span className="input-group-text">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="16"
                  height="16"
                  fill="currentColor"
                  className={style["svg"]}
                  viewBox="0 0 16 16"
                >
                  <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" />
                </svg>
              </span>
              <input
                value={formik.values.username}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                type="text"
                className="form-control"
                name="username"
                placeholder="Username"
                id="inputUsername"
              />
            </div>
            {formik.touched.username && (
              <p className="text-danger">{formik.errors.username}</p>
            )}
          </div>

          <div className="mb-3">
            <label htmlFor="inputPassword" className="form-label">
              Password
            </label>
            <div className="input-group">
              <span className="input-group-text">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="16"
                  height="16"
                  fill="currentColor"
                  viewBox="0 0 16 16"
                  className={style["svg"]}
                >
                  <path d="M8 0a1 1 0 0 1 1 1v5.268l4.562-2.634a1 1 0 1 1 1 1.732L10 8l4.562 2.634a1 1 0 1 1-1 1.732L9 9.732V15a1 1 0 1 1-2 0V9.732l-4.562 2.634a1 1 0 1 1-1-1.732L6 8 1.438 5.366a1 1 0 0 1 1-1.732L7 6.268V1a1 1 0 0 1 1-1z" />
                </svg>
              </span>
              <input
                value={formik.values.password}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                type="password"
                className="form-control"
                name="password"
                placeholder="Password"
              />
            </div>
            {formik.touched.password && (
              <p className="text-danger">{formik.errors.password}</p>
            )}
          </div>
          <div className="mb-3 form-check">
            <input
              type="checkbox"
              className="form-check-input"
              id="rememberMe"
            />
            <label className="form-check-label" htmlFor="rememberMe">
              Remember Me
            </label>
          </div>
          <button type="submit" className="btn btn-primary w-50 m-auto d-block">
            Submit
          </button>
        </form>
        <div className="orLogin text-center">
          <p className="my-2">or sign in with</p>

          {/* <loginGoogle /> */}
          <div>
            <GoogleLogin
              clientId="204046050480-qqr2qlj5r6k361kpuce9pfqek7a0d1d1.apps.googleusercontent.com"
              buttonText="Login with Terralogic Account"
              onSuccess={responseGoogle}
              cookiePolicy={"single_host_origin"}
              isSignedIn={false}
            ></GoogleLogin>
            <br />
            <br />
          </div>
        </div>
      </div>
    </div>
    //   )}
    // </Fragment>
  );
};

export default Signin;
