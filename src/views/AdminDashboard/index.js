import React, { useEffect, useState } from "react";
import "./style.css";
import Dragger from "../../components/Dragger";
import { PopupModal } from "../../components/PopupModal";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteDoc,
  fetchDetailDoc,
  fetchDoc,
} from "../../store/actions/document";
import createAction from "../../store/actions";
import { actionType } from "../../store/actions/type";
import Panigation from "../../components/Panigation";
import TableAdmin from "./tablestyle";
import CardAdmin from "./cardstyle";
import { fetchMe } from "../../store/actions/auth";

const AdminDashboard = () => {
  // State
  const [isOpen, setOpen] = useState(false);

  const [isCard, setCard] = useState(false);
  const [perPage, setPerPage] = useState(4);
  const dispatch = useDispatch();

  const { dataDocument, dataDocumentPageIndex } = useSelector(
    (state) => state.DataDocument
  );

  //render table

  useEffect(() => {
    dispatch(fetchDoc(perPage, dataDocumentPageIndex));
  }, [dispatch, dataDocumentPageIndex, perPage]);

  useEffect(() => {
    dispatch(fetchMe);
  }, []);
  //

  const handleChangeDocPerPage = (e) => {
    setPerPage(e.target.value);
  };

  //Assign document feature
  const handleAssignDocument = (id) => {
    setOpen(true);
    dispatch(
      createAction(actionType.SET_ASSIGN_DOCUMENT_STATE, {
        state: true,
        id,
      })
    );
  };

  // View document feature
  const handleViewDocument = (id) => {
    setOpen(true);
    dispatch(fetchDetailDoc(id));
    dispatch(createAction(actionType.SET_DETAIL_DOCUMENT_STATE, true));
  };
  //

  //Edit document feature
  const handleEditDocument = (id) => {
    setOpen(true);
    dispatch(
      createAction(actionType.SET_EDIT_DOCUMENT_STATE, {
        state: true,
        id,
      })
    );
  };
  //

  //Delete document feature
  const handleDelete = async (id, title) => {
    if (window.confirm(`Are you sure to delete ${title} document ?`)) {
      dispatch(deleteDoc(id, perPage, dataDocumentPageIndex));
    } else {
      return;
    }
  };
  //

  // Modal
  const closedModal = () => {
    setOpen(false);
    dispatch(createAction(actionType.SET_CLEAR_MODAL_CONTENT_STATE));
  };

  //

  return (
    // <Fragment>
    //   {!token ? (
    //     <Navigate replace to="/Signin" />
    //   ) : (
    <div className="container">
      <div className="toggle d-flex justify-content-end my-2">
        <button
          onClick={() => setCard(false)}
          type="button"
          className="btn btn-primary "
          title="Table style"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="20"
            height="20"
            fill="currentColor"
            viewBox="0 0 16 16"
          >
            <path d="M0 2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2zm15 2h-4v3h4V4zm0 4h-4v3h4V8zm0 4h-4v3h3a1 1 0 0 0 1-1v-2zm-5 3v-3H6v3h4zm-5 0v-3H1v2a1 1 0 0 0 1 1h3zm-4-4h4V8H1v3zm0-4h4V4H1v3zm5-3v3h4V4H6zm4 4H6v3h4V8z" />
          </svg>
        </button>

        <button
          onClick={() => setCard(true)}
          type="button"
          className="btn btn-primary"
          title="Card style"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="25"
            height="25"
            fill="white"
            viewBox="0 0 16 16"
          >
            <path d="M14.5 3a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-13a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h13zm-13-1A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-13z" />
            <path d="M3 5.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zM3 8a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9A.5.5 0 0 1 3 8zm0 2.5a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5z" />
          </svg>
        </button>
      </div>
      <Dragger perPage={perPage} />
      {isCard ? (
        <CardAdmin
          handleAssignDocument={handleAssignDocument}
          handleViewDocument={handleViewDocument}
          handleEditDocument={handleEditDocument}
          handleDelete={handleDelete}
          dataDocument={dataDocument}
        />
      ) : (
        <TableAdmin
          perPage={perPage}
          handleAssignDocument={handleAssignDocument}
          handleViewDocument={handleViewDocument}
          handleEditDocument={handleEditDocument}
          handleDelete={handleDelete}
          dataDocument={dataDocument}
        />
      )}
      <div>
        <Panigation />
        <select
          onChange={(e) => handleChangeDocPerPage(e)}
          style={{ width: "6.5rem" }}
          class="form-select form-select-sm m-auto"
        >
          <option value="4">4 / page</option>
          <option value="8">8 / page</option>
          <option value="12">12 / page</option>
        </select>
      </div>

      <PopupModal isOpen={isOpen} closed={closedModal} />
    </div>
    //   )}
    // </Fragment>
  );
};

export default AdminDashboard;
