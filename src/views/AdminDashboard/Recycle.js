import React, { Fragment, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Panigation from "../../components/Panigation";
import { fetchMe } from "../../store/actions/auth";
import { fetchTrash } from "../../store/actions/document";
import Loading from "../../components/loading";
import { SkeletonTable } from "../../components/Skeleton";

const Recycle = () => {
  const { deleteDocumentList, dataDocumentPageIndex, loadingState } =
    useSelector((state) => state.DataDocument);
  const [perPage, setPerPage] = useState(4);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchTrash(perPage, dataDocumentPageIndex));
  }, [perPage, dataDocumentPageIndex]);

  useEffect(() => {
    dispatch(fetchMe);
  }, []);

  const handleRestore = () => {
    dispatch();
  };

  const handleChangeDocPerPage = (e) => {
    setPerPage(e.target.value);
  };

  return (
    <Fragment>
      {loadingState ? (
        <SkeletonTable />
      ) : (
        <div className="container table-responsive">
          <table className="table table-responsive-sm table-bordered text-center ">
            <thead className="table-secondary">
              <tr>
                <th>Document Name</th>
                <th>Created Date</th>
                <th>Last Deleted Date</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {deleteDocumentList.map((item) => {
                const { _id, title, deletedAt, createdAt } = item;
                return (
                  <tr key={_id}>
                    <td>{title}</td>
                    <td>
                      {new Date(createdAt).toLocaleDateString("en-vi", {
                        day: "numeric",
                        year: "numeric",
                        month: "short",
                      })}
                    </td>
                    <td>
                      {new Date(deletedAt).toLocaleDateString("en-vi", {
                        day: "numeric",
                        year: "numeric",
                        month: "short",
                      })}
                    </td>
                    <td>
                      <span onClick={() => handleRestore(_id)}>Restore</span>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
          <Panigation />
          <select
            onChange={(e) => handleChangeDocPerPage(e)}
            style={{ width: "6.5rem" }}
            class="form-select form-select-sm m-auto"
          >
            <option value="4">4 / page</option>
            <option value="8">8 / page</option>
            <option value="12">12 / page</option>
          </select>
        </div>
      )}
    </Fragment>
  );
};

export default Recycle;
