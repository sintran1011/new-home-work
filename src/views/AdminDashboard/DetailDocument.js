import React, { Fragment, useState, useEffect } from "react";
import { useSelector } from "react-redux";
import Loading from "../../components/loading";
import { DOMAIN_IMG } from "../../util/config";
import { Document, Page, pdfjs } from "react-pdf";
pdfjs.GlobalWorkerOptions.workerSrc = `https://cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

const DetailDocument = () => {
  const { detailDocument, loadingState } = useSelector(
    (state) => state.DataDocument
  );

  // react-pdf set up

  const [numPages, setNumPages] = useState(null);
  const [pageNumber, setPageNumber] = useState(1);

  function onDocumentLoadSuccess({ numPages }) {
    setNumPages(numPages);
    setPageNumber(1);
  }

  //

  const token = localStorage.getItem("token");

  console.log(`${DOMAIN_IMG}/${detailDocument.url}`);

  return (
    <Fragment>
      <Document
        className="w-100 h-100 p-3"
        file={{
          url: `${DOMAIN_IMG}/${detailDocument.url}`,
          httpHeaders: {
            Authorization: `Bearer ${token}`,
          },
        }}
        loading={<Loading />}
        onLoadSuccess={onDocumentLoadSuccess}
        onLoadError={console.error}
      >
        {Array.from(new Array(numPages), (el, index) => (
          <Page key={`page_${index + 1}`} pageNumber={index + 1} />
        ))}
      </Document>
    </Fragment>
  );
};

export default DetailDocument;
