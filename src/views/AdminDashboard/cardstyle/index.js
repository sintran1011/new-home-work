import React, { Fragment } from "react";
import { useSelector } from "react-redux";
import Loading from "../../../components/loading";

const CardAdmin = (props) => {
  const {
    dataDocument,
    handleAssignDocument,
    handleViewDocument,
    handleEditDocument,
    handleDelete,
  } = props;

  const { loadingState } = useSelector((state) => state.DataDocument);

  return (
    <Fragment>
      {loadingState ? (
        <Loading />
      ) : (
        <div className="row row-cols-1 row-cols-md-4 g-2 w-100 m-auto">
          {dataDocument
            ? dataDocument.map((item) => {
                const { _id, title, updatedAt } = item;
                return (
                  <div key={_id} className="col">
                    <div className="card h-100">
                      <div className="card-body">
                        <h5 className="card-title">{title}</h5>
                        <p className="card-text">
                          UpdateDate:{" "}
                          {new Date(updatedAt).toLocaleDateString("en-vi", {
                            day: "numeric",
                            year: "numeric",
                            month: "short",
                          })}
                        </p>
                        <div className="d-flex w-75 justify-content-between">
                          <button
                            onClick={() => handleAssignDocument(_id)}
                            type="button"
                            className="btn btn-success"
                          >
                            Assign
                          </button>
                          <button
                            onClick={() => handleViewDocument(_id)}
                            type="button"
                            className="btn btn-primary"
                          >
                            View
                          </button>
                          <button
                            onClick={() => handleEditDocument(_id)}
                            type="button"
                            className="btn btn-secondary"
                          >
                            Edit
                          </button>
                          <button
                            onClick={() => handleDelete(_id)}
                            type="button"
                            className="btn btn-danger"
                          >
                            Delete
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })
            : null}
        </div>
      )}
    </Fragment>
  );
};

export default CardAdmin;
