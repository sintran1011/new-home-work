import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateDoc } from "../../store/actions/document";

const EditDocument = (props) => {
  const dispatch = useDispatch();
  const title = "Click or Drag the file here to Upload a new Document";

  const { editDocumentId, dataDocumentPerPage, dataDocumentPageIndex } =
    useSelector((state) => state.DataDocument);
  const { closed } = props;

  // statement
  const [fileName, setFileName] = useState(title);
  const [file, setFile] = useState("");
  const [isFile, setIsFile] = useState(false);
  //

  //drag and drop feature
  const handleDrop = (e) => {
    let files = e.target.files[0];
    let fileName = files.name;
    setFile(files);
    setFileName(fileName);
    setIsFile(true);
    alert("upload file succeed");
  };
  //

  //Upload feature
  const handleUpload = () => {
    const data = new FormData();
    data.append("file", file);
    console.log(data);
    dispatch(
      updateDoc(
        editDocumentId,
        data,
        dataDocumentPerPage,
        dataDocumentPageIndex
      )
    );
    closed();
  };
  //

  return (
    <div className="container">
      <form className="text-center w-75 m-auto center ">
        <h1>Edit document</h1>
        <div className="drop-area">
          <div className="input-content">
            <input
              type="file"
              id="fileInput"
              multiple
              accept=".doc,.docx,.pdf,.jpg"
              onChange={(e) => handleDrop(e)}
            />
            <p className="text-decoration-underline text-primary">{fileName}</p>
          </div>

          {isFile ? (
            <div onClick={handleUpload} className="btn btn-success mt-3">
              Upload
            </div>
          ) : null}
        </div>
      </form>
    </div>
  );
};

export default EditDocument;
