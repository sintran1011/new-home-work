import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateListUserReadDoc } from "../../store/actions/document";
import AssignTable from "./AssignTable";

const AssignDocument = () => {
  // const dispatch = useDispatch();

  const [isAssign, setAssign] = useState("");

  // const { assignDocumentId } = useSelector((state) => state.DataDocument);

  // check for push userId to array have permision reac doc
  // const handleCheck = (e, id) => {
  //   if (e.target.checked) {
  //     setIdUser([...idUser, id]);
  //   } else {
  //     const idList = idUser.filter((item) => {
  //       return item !== id;
  //     });
  //     setIdUser(idList);
  //   }
  // };

  const handleChange = (state) => {
    setAssign(state);
  };

  // //upload doc
  // const handleUpload = () => {
  //   //convert user array to form BE give
  //   let userObjects = [];
  //   let confirms = {};

  //   idUser.forEach((item) => userObjects.push({ userId: item }));

  //   confirms = {
  //     docId: assignDocumentId,
  //     users: userObjects,
  //     active: !isAssign,
  //   };

  //   dispatch(updateListUserReadDoc(confirms, assignDocumentId, isAssign));

  //   setIdUser([]);
  //   console.log(confirms);
  //   console.log(userObjects);
  // };

  return (
    <div className="m-auto w-75 pt-5">
      <div className="mb-2">
        <button
          onClick={() => handleChange(false)}
          className="btn btn-primary mr-5"
        >
          Assign
        </button>
        <button onClick={() => handleChange(true)} className="btn btn-danger">
          UnAssign
        </button>
      </div>
      <AssignTable isAssign={isAssign} />
    </div>
  );
};

export default AssignDocument;
