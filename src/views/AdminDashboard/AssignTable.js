import React, { Fragment, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Loading from "../../components/loading";
import {
  getUserListAssign,
  updateListUserReadDoc,
} from "../../store/actions/document";

const AssignTable = (props) => {
  const { isAssign } = props;
  const [idUser, setIdUser] = useState([]);
  const { userListAssign, loadingState, assignDocumentId } = useSelector(
    (state) => state.DataDocument
  );

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getUserListAssign(assignDocumentId, isAssign));
  }, [dispatch, assignDocumentId, isAssign]);

  //upload doc
  const handleUpload = () => {
    //convert user array to form BE give
    let userObjects = [];
    let confirms = {};

    idUser.forEach((item) => userObjects.push({ userId: item }));

    confirms = {
      docId: assignDocumentId,
      users: userObjects,
      active: !isAssign,
    };

    dispatch(updateListUserReadDoc(confirms, assignDocumentId, isAssign));

    setIdUser([]);
    console.log(confirms);
    console.log(userObjects);
  };

  // check for push userId to array have permision reac doc
  const handleCheck = (e, id) => {
    if (e.target.checked) {
      setIdUser([...idUser, id]);
    } else {
      const idList = idUser.filter((item) => {
        return item !== id;
      });
      setIdUser(idList);
    }
  };

  return (
    <Fragment>
      <table className="table table-bordered text-center">
        <thead className="table-secondary">
          <tr>
            <th>Username</th>
            <th>Check</th>
            {isAssign ? <th>Status</th> : null}
          </tr>
        </thead>
        <tbody>
          {loadingState ? <Loading /> : null}
          {userListAssign.users
            ? userListAssign.users.map((item) => {
                const { name, userId, status } = item;
                return (
                  <tr key={userId}>
                    <td>{name}</td>
                    <td>
                      <input
                        onChange={(e) => handleCheck(e, userId)}
                        type="checkbox"
                      />
                    </td>
                    {isAssign ? <td>{status}</td> : null}
                  </tr>
                );
              })
            : null}
        </tbody>
      </table>
      <div className="text-center">
        <button onClick={handleUpload} className="btn btn-success">
          Confirm
        </button>
      </div>
    </Fragment>
  );
};

export default AssignTable;
