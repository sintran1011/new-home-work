import React, { Fragment } from "react";
import { useSelector } from "react-redux";
import DeleteIcon from "../../../components/BSIcon/DeleteIcon";
import EyeIcon from "../../../components/BSIcon/EyeIcon";
import SettingIcon from "../../../components/BSIcon/SettingIcon";
import { SkeletonTable } from "../../../components/Skeleton";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";

const TableAdmin = (props) => {
  const {
    dataDocument,
    handleAssignDocument,
    handleViewDocument,
    handleEditDocument,
    handleDelete,
    perPage,
  } = props;

  const { loadingState } = useSelector((state) => state.DataDocument);
  return (
    <Fragment>
      {loadingState ? (
        <SkeletonTable perPage={perPage} />
      ) : (
        <div className="table-responsive">
          <table className="table table-responsive-sm table-bordered text-center ">
            <thead className="table-secondary">
              <tr>
                <th>Document Name</th>
                <th>Update Date</th>
                <th>Assign</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody>
              {dataDocument.map((item) => {
                const { _id, title, updatedAt } = item;
                return (
                  <tr key={_id}>
                    <td>{loadingState ? <Skeleton /> : title}</td>
                    <td>
                      {loadingState ? (
                        <Skeleton />
                      ) : (
                        new Date(updatedAt).toLocaleDateString("en-vi", {
                          day: "numeric",
                          year: "numeric",
                          month: "short",
                        })
                      )}
                    </td>
                    <td>
                      <span onClick={() => handleAssignDocument(_id)}>
                        {loadingState ? <Skeleton /> : "Assign"}
                      </span>
                    </td>
                    <td>
                      {loadingState ? (
                        <Skeleton />
                      ) : (
                        <>
                          <span onClick={() => handleViewDocument(_id)}>
                            <EyeIcon />
                          </span>

                          <span
                            className="px-1"
                            onClick={() => handleEditDocument(_id)}
                          >
                            <SettingIcon />
                          </span>
                          <span onClick={() => handleDelete(_id, title)}>
                            <DeleteIcon />
                          </span>
                        </>
                      )}
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      )}
    </Fragment>
  );
};

export default TableAdmin;
