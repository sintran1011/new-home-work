import React from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import "./CardGrid.css";
import { Button, Modal } from "react-bootstrap";
import { GridModalAction } from "../../store/actions/GridModalAction";

export default function CardGrid(props) {
  const { arrUserDB } = useSelector((state) => state.UserDBReducer);
  // console.log("nahn dc", arrUserDB);
  const arrGrid = arrUserDB.documents;
  const dispatch = useDispatch();
  const [showGird, setShowGrid] = useState(false);

  const handleCloseGrid = (docId, status) => {
    setShowGrid(false);
    dispatch(GridModalAction(docId, status));
    console.log(docId, status);
  };
  const handleShowGrid = () => setShowGrid(true);
  return (
    <div className="row" id="itemGrid">
      {arrGrid?.map((item, index) => (
        <div className="col-sm-4 col-md-col-4 col-xl-3 py-3" key={index}>
          <div className="card set-card">
            <div className="card-body  ">
              <div className="card_icon ">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={40}
                  height={40}
                  viewBox="0 0 24 24"
                  fill="#9ea8b3"
                  stroke="#4d5862"
                  strokeWidth={2}
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  className="feather feather-folder"
                >
                  <path d="M22 19a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h5l2 3h9a2 2 0 0 1 2 2z" />
                </svg>
              </div>
              <div className="card-title ">
                <h5 className="overlowText">{item?.title}</h5>
                <p> {new Date(item?.updatedAt).toLocaleDateString("vi-VI")}</p>
                <p>{item?.status}</p>
              </div>
            </div>
          </div>
          <div className="card-footer text-center">
            <span onClick={handleShowGrid} className=" eyeHover">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="30"
                height="30"
                viewBox="0 0 24 24"
                fill="none"
                stroke="currentColor"
                stroke-width="2"
                stroke-linecap="round"
                stroke-linejoin="round"
                className="feather feather-eye"
              >
                <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                <circle cx="12" cy="12" r="3"></circle>
              </svg>
            </span>
            <Modal
              show={showGird}
              onHide={handleCloseGrid}
              size="xl"
              className="gird_modal"
            >
              <Modal.Header closeButton>
                <Modal.Title>View document</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <iframe
                  src={`https://thangpham.ngrok.io/${item?.url}`}
                  frameBorder="0"
                ></iframe>
              </Modal.Body>
              <Modal.Footer>
                <Button variant="secondary" onClick={handleCloseGrid}>
                  Close
                </Button>
                <Button
                  variant="primary"
                  onClick={() => handleCloseGrid(item?.docId, item?.status)}
                >
                  Comform
                </Button>
              </Modal.Footer>
            </Modal>
          </div>
        </div>
      ))}
    </div>
  );
}
