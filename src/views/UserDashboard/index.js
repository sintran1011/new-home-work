import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import EyeModal from "../../components/ModalUserEye/EyeModal";
import "./UserDB.css";
import Axios from "axios";
import { Table, Tabs } from "react-bootstrap";
import { Tab } from "bootstrap";
import CardGrid from "./CardGrid";

export default function UserDashboard(props) {
  const { arrUserDB } = useSelector((state) => state.UserDBReducer);
  const { userGoogle } = useSelector((state) => state.UserDBReducer);
  const dispatch = useDispatch();
  const [page, setPage] = useState(1);
  const [key, setKey] = useState("table");
  useEffect(async () => {
    try {
      const result = await Axios({
        url: `https://thangpham.ngrok.io/api/v1/document?perPage=5&page=${page}`,
        method: "GET",
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token"),
        },
      });
      //đưa len reducer

      dispatch({
        type: "GET_DSUSER",
        arrUserDB: result.data,
      });
    } catch (errors) {
      console.log("errors", errors);
    }
  }, [page]);

  const arrLsit = arrUserDB.documents;
  console.log("ten nguoi dung", userGoogle);
  return (
    <div className="container py-5 position-relative ">
      <div>
        <div className="page-content d-flex flex-column justify-content-between px-4 flex-grow-1 text-center">
          <Tabs
            defaultActiveKey="table"
            transition={false}
            id="noanim-tab-example"
            className="mb-3"
          >
            <Tab eventKey="table" title="Table">
              {/* striped bordered hover */}
              <Table striped bordered hover responsive="md">
                <thead className="table-secondary">
                  <tr>
                    <th scope="col">Document Name</th>
                    <th scope="col">Assigned Date</th>
                    <th scope="col">View</th>
                    <th scope="col">Status</th>
                  </tr>
                </thead>
                <tbody>
                  {arrLsit?.map((item, index) => (
                    <tr key={index}>
                      <td className="col">{item?.title}</td>
                      <td className="col">
                        {new Date(item?.updatedAt).toLocaleDateString("vi-VI")}
                      </td>
                      <td className="col">
                        <p
                        // onClick={() => {
                        //   handleView(item.docId);
                        // }}
                        >
                          <EyeModal item={item} />
                        </p>
                      </td>
                      <td className="col">{item?.status}</td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </Tab>
            <Tab eventKey="grid" title="Grid">
              <div className="my-3">
                <CardGrid />
              </div>
            </Tab>
          </Tabs>
          <nav id="panigationUS" aria-label="Page navigation example ">
            <ul className="pagination justify-content-center text-dark">
              <li className="page-item">
                <a
                  className="page-link"
                  href="#"
                  onClick={() => {
                    if (page == 1) {
                      setPage((page = 1));
                    } else {
                      setPage(page - 1);
                    }
                  }}
                >
                  &laquo;
                </a>
              </li>
              <li className="page-item">
                <a
                  className="page-link "
                  href="#"
                  onClick={() => {
                    setPage(1);
                  }}
                >
                  1
                </a>
              </li>
              <li className="page-item">
                <a
                  className="page-link"
                  href="#"
                  onClick={() => {
                    setPage(2);
                  }}
                >
                  2
                </a>
              </li>
              <li className="page-item">
                <a
                  className="page-link"
                  href="#"
                  onClick={() => {
                    setPage(3);
                  }}
                >
                  3
                </a>
              </li>
              <li className="page-item">
                <a
                  className="page-link"
                  href="#"
                  onClick={() => {
                    setPage(page + 1);
                  }}
                >
                  &raquo;
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  );
}
