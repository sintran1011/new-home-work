export const DOMAIN = "https://thangpham.ngrok.io/api/v1";

export const DOMAIN_IMG = "https://thangpham.ngrok.io";

export const token = () => {
  return localStorage.getItem("token");
};
