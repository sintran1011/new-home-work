import React, { useState } from "react";
import EyeIcon from "../BSIcon/EyeIcon";
import { Button, Modal } from "react-bootstrap";
import "./EyeModal.css";
import { useDispatch } from "react-redux";
import { ModalViewUser } from "../../store/actions/ModalViewUserAction";
export default function EyeModal(props) {
  const [show, setShow] = useState(false);
  const dispatch = useDispatch();
  const handleClose = (docId, status) => {
    setShow(false);
    dispatch(ModalViewUser(docId, status));
    // console.log(docId);
  };
  const handleShow = () => setShow(true);
  // console.log("sdsdda", props.item);
  return (
    <div id="eye_modal">
      <>
        <p onClick={handleShow}>
          <EyeIcon />
        </p>

        <Modal
          show={show}
          onHide={handleClose}
          size="xl"
          className="table_modal"
        >
          <Modal.Header closeButton>
            <Modal.Title>View document</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {/* <iframe src={props.item?.docId.url} frameborder="0"></iframe> */}
            <iframe
              src={`https://thangpham.ngrok.io/${props?.item.url}`}
              frameBorder="0"
            ></iframe>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={() => handleClose()}>
              Close
            </Button>
            <Button
              variant="primary"
              onClick={() => handleClose(props?.item.docId, props?.item.status)}
            >
              Comform
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    </div>
  );
}
