import React, { useState } from "react";
import "./style.css";

const Loading = () => {
  return (
    <div className="wrapper">
      <div className="spinner">
        <i></i>
        <i></i>
        <i></i>
        <i></i>
        <i></i>
        <i></i>
        <i></i>
      </div>
    </div>
  );
};

export default Loading;
