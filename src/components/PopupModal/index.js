import React from "react";
import { useSelector } from "react-redux";
import AssignDocument from "../../views/AdminDashboard/AssignDocument";
import DetailDocument from "../../views/AdminDashboard/DetailDocument";
import EditDocument from "../../views/AdminDashboard/EditDocument";
import "./style.css";

export const PopupModal = (props) => {
  const { isOpen, closed } = props;

  const { editDocumentState, detailDocumentState, assignDocumentState } =
    useSelector((state) => state.DataDocument);

  return (
    <div>
      <div
        onClick={closed}
        id="activePopup-overlay"
        className={`popup-overlay ${isOpen ? "activePopup-overlay" : ""}`}
      />
      <div id="activePopup" className={`popup ${isOpen ? "activePopup" : ""}`}>
        {detailDocumentState ? <DetailDocument /> : null}
        {editDocumentState ? <EditDocument closed={closed} /> : null}
        {assignDocumentState ? <AssignDocument /> : null}
      </div>
    </div>
  );
};
