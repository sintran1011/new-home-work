import React from "react";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";

export const SkeletonTable = (props) => {
  let { perPage } = props;
  perPage = Number(perPage);

  return (
    <div className="table-responsive">
      <table className="table table-responsive-sm table-bordered text-center ">
        <thead className="table-secondary">
          <tr>
            {new Array(4).fill().map((item) => {
              return (
                <th>
                  <Skeleton />
                </th>
              );
            })}
          </tr>
        </thead>
        <tbody>
          {new Array(perPage).fill().map((item) => {
            return (
              <tr>
                {new Array(4).fill().map((item) => {
                  return (
                    <td>
                      <Skeleton />
                    </td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};
