import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addNewDoc } from "../../store/actions/document";
import "./style.css";

const Dragger = (props) => {
  const { perPage } = props;
  const { dataDocumentPageIndex } = useSelector((state) => state.DataDocument);

  const [fileName, setFileName] = useState(
    "Click or Drag the file here to upload new Document"
  );
  const [file, setFile] = useState("");
  const [isFile, setIsFile] = useState(false);

  const dispatch = useDispatch();

  //drag and drop feature
  const handleDrop = (e) => {
    let files = e.target.files[0];
    let fileName = files.name;
    setFile(files);
    setFileName(fileName);
    setIsFile(true);
    alert("import file succeed");
  };
  //

  //Upload feature
  const handleUpload = () => {
    const data = new FormData();
    data.append("file", file);
    console.log(data);
    dispatch(addNewDoc(data, perPage, dataDocumentPageIndex));
    setFile(null);
    setIsFile(false);
    setFileName("Click or Drag the file here to upload new Document");
  };
  //

  return (
    <div className="drop-area">
      <div className="input-content">
        <input
          type="file"
          id="fileInput"
          multiple
          accept=".doc,.docx,.pdf,.jpg"
          onChange={(e) => handleDrop(e)}
        />
        <div className={isFile ? "uploadContent" : null}>
          <p className="text-primary">{fileName}</p>
          {isFile ? (
            <div>
              <button
                onClick={handleUpload}
                className="btn btn-success mt-2 d-block m-auto"
              >
                Upload
              </button>
            </div>
          ) : null}
        </div>
      </div>
    </div>
  );
};

export default Dragger;
