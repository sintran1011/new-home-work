import { computeHeadingLevel } from "@testing-library/react";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import createAction from "../../store/actions";
import { actionType } from "../../store/actions/type";
import "./style.css";

const Panigation = () => {
  const dispatch = useDispatch();
  const [curentPage, setCurrentPage] = useState(null);

  const { dataDocumentPages } = useSelector((state) => state.DataDocument);

  // panigation
  let list = [];
  const fetchPageIndex = () => {
    for (let i = 1; i <= dataDocumentPages; i++) {
      list.push(i);
    }
  };

  fetchPageIndex();
  const page = document.getElementById(`page_${curentPage}`);

  //active pageIndex on panigation
  const pageList = document.querySelectorAll(".page-index");
  console.log(page);
  function activePage() {
    pageList.forEach((item) => {
      item.classList.remove("active");
      this.classList.add("active");
    });
  }

  pageList.forEach((item) => {
    item.addEventListener("click", activePage);
  });

  const handleChange = (item) => {
    setCurrentPage(item);
    dispatch(createAction(actionType.SET_PAGE_INDEX, item));
  };

  return (
    <nav aria-label="Page navigation cursor-pointer ">
      <ul className="pagination justify-content-center my-2">
        <li className="page-item ">
          <a href="#!" className="page-link" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
          </a>
        </li>
        {list.map((item, index) => {
          return (
            <li
              id={`page_${item}`}
              key={index}
              className="page-item page-index cursor-pointer"
            >
              <a
                onClick={() => handleChange(item)}
                href="#!"
                className="page-link page-item"
              >
                {item}
              </a>
            </li>
          );
        })}

        <li className="page-item">
          <a href="#!" className="page-link" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
          </a>
        </li>
      </ul>
    </nav>
  );
};

export default Panigation;
