import React from "react";
import { UserIcon } from "../BSIcon/UserIcon";
import TLlogo from "../../img/header-logo.svg";
import style from "./style.module.css";
import { useSelector } from "react-redux";

const Header = () => {
  // state
  const name = useSelector((state) => state.Me?.name);
  const token = localStorage.getItem("token");

  const logOut = () => {
    localStorage.removeItem("token");
    window.location.reload();
  };

  return (
    <div className="shadow-sm mb-3 py-2">
      <div className="container">
        <div className="d-flex align-items-center justify-content-between">
          <img className={`d-block ${style["logo"]}`} src={TLlogo} />
          {token ? (
            <div className="d-flex justify-content-end">
              <span className={`${style["span"]} d-inline-block mr-2`}>
                <img
                  className="rounded-circle"
                  src={`https://picsum.photos/40/40`}
                />{" "}
                <span className="pr-2">{name}</span>
              </span>

              <span
                className={`${style["span"]} ${style["logout"]}`}
                onClick={logOut}
              >
                || Sign out
              </span>
            </div>
          ) : null}
        </div>
      </div>
    </div>
  );
};

export default Header;
