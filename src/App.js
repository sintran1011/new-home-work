import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import Layout from "./HOCs/layout/Layout";
import Home from "./views/Home";
import AdminDashboard from "./views/AdminDashboard";
import Signin from "./views/Signin";
import UserDashboard from "./views/UserDashboard";
import { AuthRoute, LoginRoute } from "./HOCs/route/";
import Recycle from "./views/AdminDashboard/Recycle";
import {SkeletonTable} from "./components/Skeleton";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index element={<Home />} />
          <Route
            path="Signin"
            element={
              <LoginRoute navigativePath="/Admin">
                <Signin />
              </LoginRoute>
            }
          />

          <Route
            path="Admin"
            element={
              <AuthRoute navigativePath="/Signin">
                <AdminDashboard />
              </AuthRoute>
            }
          ></Route>

          <Route path="Restore" element={<Recycle />} />
          <Route path="Skeleton" element={<SkeletonTable />} />
          {/* <Route path="Signin" element={<Signin />} /> */}
          {/* <Route path="Admin" element={<AdminDashboard />} /> */}

          <Route path="User" element={<UserDashboard />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
