import { baseService } from ".";

class document extends baseService {
  getAllDoc = (perPage, pageIndex) => {
    return this.get(`document/all?perPage=${perPage}&page=${pageIndex}`);
  };

  getDocById = (id) => {
    return this.get(`document/${id}`);
  };

  deleteDocById = (id) => {
    return this.delete(`document/${id}`);
  };

  updateDocById = (id, formData) => {
    return this.put(`document/${id}`, formData);
  };

  createNewDoc = (formData) => {
    return this.post(`document`, formData);
  };

  assignDoc = (id, state) => {
    return this.get(`user/${id}?perPage=10&page=1&active=${state}`);
  };

  allowUserReadDoc = (userData) => {
    return this.post(`confirm`, userData);
  };

  getTrash = (perPage, pageIndex) => {
    return this.get(`document/trash?perPage=${perPage}&page=${pageIndex}`);
  };
}

const doc = new document();

export const {
  getAllDoc,
  getDocById,
  deleteDocById,
  createNewDoc,
  updateDocById,
  assignDoc,
  allowUserReadDoc,
  getTrash,
} = doc;
