import { baseService } from ".";

export class userGoogle extends baseService {
  constructor() {
    super();
  }

  GGsignin = (infoLoginGoolge) => {
    return this.post(`auth/loginGoogle`, infoLoginGoolge);
  };
}

export const UserGoogle = new userGoogle();
