import { baseService } from ".";

class adminAccount extends baseService {
  signinAdmin = (userData) => {
    return this.post(`auth/admin`, userData);
  };

  accountInfo = () => {
    return this.get(`user/info`);
  };
}

const admin = new adminAccount();

export const { signinAdmin } = admin;

export const { accountInfo } = admin;
