import { baseService } from ".";

export class modalView extends baseService {
  constructor() {
    super();
  }

  popupView = (docId, status) => {
    return this.putUser(`confirm`, { docId: docId, status: "Complete" });
  };
}

export const ModalView = new modalView();
